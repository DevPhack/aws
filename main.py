"""
App main file
"""
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def index():
    return {"message": "Welcome to test aws deployed api"}


@app.post("/post/{data}")
def create(data: int):
    return {"data": data}


@app.get('/hello/{msg}')
def hello(msg: str):
    return {"msg": msg}
