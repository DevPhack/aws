from fastapi.testclient import TestClient
import pytest


@pytest.fixture
def client():
    from main import app
    client = TestClient(app=app)
    return client


def test_index(client):
    response = client.get('/')
    assert response.status_code == 200


def test_create(client):
    response = client.post("/post/35")
    assert response.status_code == 200
    assert response.json() == {"data": 35}


def test_hello(client):
    response = client.get("/hello/salut")
    assert response.status_code == 200
    assert response.json() == {"msg": "salut"}

